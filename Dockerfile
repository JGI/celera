FROM ubuntu:16.04 
#FROM debian:jessie
LABEL Maintainer Shijie Yao, syao@lbl.gov

# create download and move into it
WORKDIR /workdir

# make alias active
RUN sed -i 's/# alias/alias/' ~/.bashrc

# install the needed programs
ENV PACKAGES wget tar 
RUN apt-get update && apt-get install --yes ${PACKAGES}

# add celera bin to path
ENV PATH $PATH:/Linux-amd64/bin

# install make ...
RUN apt-get install build-essential --yes && \
    apt-get install lbzip2 --yes && \
    apt-get install libz-dev --yes && \
    apt-get install libbz2-dev --yes && \
    wget https://sourceforge.net/projects/wgs-assembler/files/wgs-assembler/wgs-8.3/wgs-8.3rc2.tar.bz2 && \
    lbzip2 -d wgs-8.3rc2.tar.bz2 && \
    tar xvf wgs-8.3rc2.tar && \
    rm wgs-8.3rc2.tar	&& \
    cd wgs-8.3rc2	&& \
    cd kmer && make install && cd .. && \
    cd src && make && cd ..	&& \
    cd ..   && \
    mv wgs-8.3rc2/Linux-amd64 /. &&\
    rm -rf wgs-8.3rc2 &&\
    apt-get remove --purge build-essential --yes  && \
    apt-get remove --purge lbzip2 --yes  && \
    apt-get remove --purge libz-dev --yes  && \
    apt-get remove --purge libbz2-dev --yes  && \
    apt-get remove --purge wget --yes

## replace the defined(%clv) with %clv to remove the error message
RUN sed -i s/defined\(%clv\)/%clv/ $(which fastaToCA) 

## caqc.pl required perl module
RUN apt-get install libstatistics-descriptive-perl --yes
