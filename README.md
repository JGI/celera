# Celera docker image #

Celera : a de novo whole-genome shotgun (WGS) DNA sequence assembler.
The image installed version 8.3rc2 (released on 24 May, 2015).
celera excutables are in program search path in the image.
To run:
docker run --rm -v --volume:/MY_PATH:/input celera:latest PROGRAM ARGS